# from .views import profileUtama, musings, schedules, schedulesview, schedulesedit, schedulesdelete
from . import views
from django.urls import path

urlpatterns = [
	path('', views.profileUtama, name='home'),
	path('musings/', views.musings, name='musings' ),
	path('schedules-add/', views.schedules_add, name='schedules_add'),
	path('schedulesView/', views.schedulesview, name ="schedulesview"),
    path('schedules/delete/<int:id>', views.schedulesdelete, name='schedulesdelete'),
]