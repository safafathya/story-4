from django import forms
from .models import Post


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['nama_kegiatan', 'tanggal', 'tempat', 'kategori']

        widgets = {
                'tanggal' : forms.DateInput(
                    attrs = {
                        'class' : 'form-control',
                        'type' : 'date',
                        'placeholder' : 'DD/MM/YYYY',
                    }
                ),

                'nama_kegiatan' : forms.TextInput(
                    attrs = {
                        'class' : 'form-control',
                        
                    }
                ),

                'tempat' : forms.TextInput(
                    attrs = {
                        'class' : 'form-control',
                        
                    }
                ),

                'kategori' : forms.Select(
                    attrs = {
                        'class' : 'form-control',
                        
                    }
                ),
            }
