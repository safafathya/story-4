from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import Post

# Create your views here.
def profileUtama(request):
	return render(request, 'story3safa.html')

def musings(request):
	return render(request, 'musings.html')	

def schedules_add(request):
	post_form = PostForm()

	if request.method == "POST":
		post_form = PostForm(request.POST)
		if post_form.is_valid():
			post_form.save()
			return redirect('schedulesview')

	context = {
		'kegiatan_form':post_form
	}

	return render(request, 'schedules_add.html', context)


def schedules(request):
	kegiatan_form = PostForm(request.POST or None)

	context  = {
		'kegiatan_form' : kegiatan_form
	}

	return render(request, 'schedules_add.html', context)
	# if request.method == 'POST':
 #    	form = PostForm(request.POST or None)
 #    if form.is_valid():
 #        form.save()
 #        return HttpResponseRedirect('/schedules')
 #    else:
 #    form = PostForm()
 #    # put render outside 'else' block, so user can see the error messages
 #    return render(request, 'schedules.html', {'form': form, 'songs': Post.objects.all()})
	# # return render(request, 'schedules.html', {'songs': Post.objects.all()})

def schedulesview(request):
    post = Post.objects.all()
    return render(request, 'schedules_view.html', {'post': post })

def schedulesedit(request, pk):
    song = get_object_or_404(Post, pk=pk)
    titleValue = Post.objects.filter(pk=pk).values('nama_kegiatan')[0];
    nama_kegiatan = titleValue['nama_kegiatan']
    
    if request.method == 'POST':
        post_form = AddForm(request.POST, instance=song)
        if post_form.is_valid():
            post_form.save()
            return render(request, 'schedules_edit.html', {'form': post_form, 'song_title': song_title })
    else:
        form = AddForm(instance=song)
    return render(request, 'schedules_edit.html', {'form': form, 'nama_kegiatan': nama_kegiatan })

def schedulesdelete(request, id):
    Post.objects.filter(id = id).delete()
    return redirect('schedulesview')	